#!/usr/bin/env python3
# encoding: utf-8

BIN_OPTABLE = {
	'+': lambda x, y: x + y,
	'-': lambda x, y: x - y,
	'*': lambda x, y: x * y,
	'/': lambda x, y: x / y,
	'**': lambda x, y: x ** y,
	'or': lambda x, y: x or y,
	'and': lambda x, y: x and y,
}

UN_OPTABLE = {
	'~': lambda x: -x,
	'not': lambda x: not x
}

class Expression(object):
	def eval(self):
		pass
	def __str__(self):
		pass

class Number(Expression):
	def __init__(self, value):
		self._value = value

	def eval(self):
		return self._value

	def __str__(self):
		return str(self._value)

class Boolean(Expression):
	def __init__(self, value):
		self._value = value

	def eval(self):
		return self._value

	def __str__(self):
		return str(self._value)

class BinaryOperator(Expression):
	def __init__(self, operator, expr1, expr2):
		self._operator = operator
		self._expr1 = expr1
		self._expr2 = expr2

	def eval(self):
		return BIN_OPTABLE[self._operator](self._expr1.eval(), self._expr2.eval())

	def __str__(self):
		return '(' + ' '.join([str(self._expr1), self._operator, str(self._expr2)]) + ')'

class UnaryOperator(Expression):
	def __init__(self, operator, expr):
		self._operator = operator
		self._expr = expr

	def eval(self):
		return UN_OPTABLE[self._operator](self._expr.eval())

	def __str__(self):
		return '(' + self._operator + ' ' + str(self._expr) + ')'

class PolishCalculator(object):
	def __init__(self):
		self._expr = None

	def eval(self, expr):
		stack = []
		for x in expr.split(' '):
			if x.isspace():
				continue
			elif x.isdigit():
				stack.append(Number(int(x)))
			elif x in ['T', 'F']:
				stack.append(Boolean(x == 'T'))
			elif x in BIN_OPTABLE.keys():
				a = stack.pop()
				b = stack.pop()
				stack.append(BinaryOperator(x, b, a))
			elif x in UN_OPTABLE.keys():
				a = stack.pop()
				stack.append(UnaryOperator(x, a))
		assert len(stack) == 1
		self._expr = stack.pop()
		return self._expr.eval()

	def __str__(self):
		return str(self._expr)

def main():
	calc = PolishCalculator()
	res = calc.eval("3 2 ~ * 2 +")
	print("{} = {}".format(str(calc), res))

if __name__ == '__main__':
	main()
