#!/usr/bin/env python3
# encoding: utf-8

class SortedDictionary(dict):
	def __init__(self, *args, **kwargs):
		super(SortedDictionary, self).__init__()
		self._sorted_keys = []
		self.update(*args, **kwargs)

	def __iter__(self):
		return self._sorted_keys.__iter__()

	def __setitem__(self, key, value):
		self._add_key(key)
		super().__setitem__(key, value)

	def __delitem__(self, key):
		if key in self._sorted_keys:
			self._sorted_keys.remove(key)
			super().__delitem__(key)

	def clear(self):
		super().clear()
		self._sorted_keys.clear()

	def copy(self):
		newone = SortedDictionary()
		newone.update(self)
		return newone

	def keys(self):
		return self._sorted_keys

	def update(self, d={}, **kwargs):
		for key in d:
			self._add_key(key)
		for key in kwargs:
			self._add_key(key)
		super().update(d, **kwargs)

	def _add_key(self, key):
		if key not in self._sorted_keys:
			self._sorted_keys.append(key)
			self._sorted_keys.sort()

	def pop(self, key, *args):
		super().pop(key, *args)
		if key in self._sorted_keys:
			self._sorted_keys.remove(key)

	def popitem(self):
		(k, v) = super().popitem()
		self._sorted_keys.remove(k)
		return (k, v)

def main():
	data = {'banana': 3, 'apple': 4, 'pear': 1, 'orange': 2}
	d = SortedDictionary(data)
	d.update(data)
	assert d.keys() == sorted(data.keys())

	data.pop('pear')
	d.pop('pear')
	assert d.keys() == sorted(data.keys())

	k, _ = d.popitem()
	data.pop(k)
	assert d.keys() == sorted(data.keys())

	d['lemon'] = 10
	data['lemon'] = 10
	assert d.keys() == sorted(data.keys())

	del d['lemon']
	del data['lemon']
	assert d.keys() == sorted(data.keys())

	assert d.copy().keys() == sorted(data.keys())

	d.clear()
	assert d.keys() == []

if __name__ == '__main__':
	main()
