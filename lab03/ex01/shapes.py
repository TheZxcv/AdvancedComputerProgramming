#!/usr/bin/env python3
# encoding: utf-8

from math import sqrt

class Triangle(object):
	def __init__(self, side):
		self._side = side
		self._height = (sqrt(3) / 2) * side

	def calculate_area(self):
		return self._side * self._height
		
	def calculate_perimeter(self):
		return 3 * self._side

	def __str__(self):
		classname = str(type(self))
		return "{}({})".format(classname[classname.rfind('.') + 1:-2], self._side)

class Rectangle(object):
	def __init__(self, width, length):
		self._width = width
		self._length = length

	def calculate_area(self):
		return self._width * self._length
		
	def calculate_perimeter(self):
		return 2 * (self._width + self._length)

	def __str__(self):
		classname = str(type(self))
		return "{}({}, {})".format(classname[classname.rfind('.') + 1:-2], self._width, self._length)

class Square(Rectangle):
	def __init__(self, side):
		super().__init__(side, side)

	def __str__(self):
		classname = str(type(self))
		return "{}({})".format(classname[classname.rfind('.') + 1:-2], self._width)

from math import pi

class Circle(object):
	def __init__(self, radius):
		self._radius = radius

	def calculate_area(self):
		return pi * (self._radius ** 2)

	def calculate_perimeter(self):
		return 2 * math.pi * self._radius

	def __str__(self):
		classname = str(type(self))
		return "{}({})".format(classname[classname.rfind('.') + 1:-2], self._radius)

class Pentagon(object):
	def __init__(self, side):
		self._side = side

	def calculate_area(self):
		return 5 * Triangle(self._side).calculate_area()

	def calculate_perimeter(self):
		return 5 * self._side

	def __str__(self):
		classname = str(type(self))
		return "{}({})".format(classname[classname.rfind('.') + 1:-2], self._side)

class Hexagon(object):
	def __init__(self, side):
		self._side = side

	def calculate_area(self):
		return 6 * Triangle(self._side).calculate_area()

	def calculate_perimeter(self):
		return 6 * self._side

	def __str__(self):
		classname = str(type(self))
		return "{}({})".format(classname[classname.rfind('.') + 1:-2], self._side)

class ShapesIterator(object):
	def __init__(self, li):
		self._li = sorted(li, key=lambda shape: shape.calculate_area(), reverse=True)

	def __iter__(self):
		return self

	def __next__(self):
		try:
			return self._li.pop()
		except IndexError:
			raise StopIteration

def main():
	shapes = [Triangle(3), Rectangle(2, 4), Square(5), Circle(6), Pentagon(7)]

	for s in ShapesIterator(shapes):
		print(s)
if __name__ == '__main__':
	main()
