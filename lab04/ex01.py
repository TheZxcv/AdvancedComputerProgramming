#!/usr/bin/env python3
# encoding: utf-8

class ImprovedString(str):
	def ispalindrome(self):
		li = [c.lower() for c in super().__str__() if c.isalpha()]
		return li == li[::-1]

	def __sub__(self, other):
		d = {}
		for c in set(other):
			d[ord(c)] = None
		return ImprovedString(self.translate(d))

	@staticmethod
	def hasanagram(strings):
		sets = [set(s) for s in strings]
		return any([sets.count(s) > 1 for s in sets])

PALINDROMES = [
	'radar',
	'anna',
	'detartrated',
	'Do geese see God?',
	'Rise to vote, sir.'
]

NOT_PALINDROMES = [
		'abc',
		'ab'
]

def main():
	for s in PALINDROMES:
		imprs = ImprovedString(s)
		assert imprs.ispalindrome(), 'Assertion failed on {}'.format(s)

	for s in NOT_PALINDROMES:
		imprs = ImprovedString(s)
		assert not imprs.ispalindrome(), 'Assertion failed on {}'.format(s)

	res = ImprovedString('This is A test case') - 'aeiou'
	assert res == 'Ths s A tst cs'

	assert ImprovedString.hasanagram(['abc', 'bca'])
	assert not ImprovedString.hasanagram(['abc', 'bc'])

if __name__ == '__main__':
	main()
