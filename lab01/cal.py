#!/usr/bin/env python
# encoding: UTF-8
from calendar import isleap, leapdays, day_name, weekday

def nextLeap(curYear):
    curYear += 1
    while True:
        if isleap(curYear):
            return curYear
        curYear += 1

def leapYears(begin, end):
    return leapdays(begin, end+1)

def dayOfTheWeek(day, month, year):
    return day_name[weekday(year, month, day)]

def main():
    nextLeapYear = nextLeap(2016)
    print("The next leap year is {}".format(nextLeapYear))
    begin, end = 2000, 2050
    cnt = leapYears(begin, end)
    print("Between {} and {} there are {} leap years".format(begin, end, cnt))

    day, month, year = 29, 7, 2016
    print(".... is a {}".format(dayOfTheWeek(day, month, year)))

if __name__ == '__main__':
    main()
