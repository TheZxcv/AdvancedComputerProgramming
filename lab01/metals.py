#!/usr/bin/env python
# encoding: UTF-8

import heapq
from pprint import pprint

METALS = [
    ('Barium', 56), 
    ('Beryllium', 4),
    ('Calcium', 20),
    ('Magnesium', 12),
    ('Radium', 88),
    ('Strontium', 38)
]

GASES = [
    ('Helium', 2),
    ('Neon', 10),
    ('Argon', 18),
    ('Krypton', 36),
    ('Xeon', 54),
    ('Radon', 86)
]

def main():
    heaviest = max(METALS, key=lambda x: x[1])
    print(heaviest)

    tmp = sorted(METALS, key=lambda x: x[1])
    print("Sorted list {}".format(tmp))

    metals = {key: value for key, value in METALS}
    pprint(metals)

    noble_gases = {key: value for key, value in GASES}
    pprint(noble_gases)

    merged = metals.copy()
    merged.update(noble_gases)
    print('Merged:')
    pprint(merged, indent=4)

    li = list(merged.items())
    li.sort(key=lambda x: x[1])
    while li != []:
        print(li.pop())
    

if __name__ == '__main__':
    main()
