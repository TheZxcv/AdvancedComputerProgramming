#!/usr/bin/env python
# encoding: UTF-8

from pprint import pprint

def identity(size):
    return [[0 if i != j else 1 for j in range(size)] for i in range(size)]

def square(size):
    return [[j + i*size for j in range(size)] for i in range(size)]

def transpose(matrix):
    if matrix == []:
        return []
    return [[ matrix[i][j] for i in range(len(matrix))] for j in range(len(matrix[0])) ]

def multiply(A, B):
    t = transpose(B)
    return [[sum(map(lambda val: val[0]*val[1], zip(li1, li2))) for li2 in t] for li1 in A]

def main():
    [pprint(identity(i)) for i in range(10)]
    [pprint(square(i)) for i in range(10)]
    [pprint(transpose(square(i))) for i in range(10)]
    [pprint(multiply(square(i), square(i))) for i in range(10)]
    pprint(multiply([[1,2,3],[4,5,6]],[[7],[8],[9]]))

if __name__ == '__main__':
    main()
