#!/usr/bin/env python
# encoding: UTF-8

import os
import sys
from sys import stdout
import shell

def open_file(command, filename):
    try:
        return open(filename, 'r')
    except IsADirectoryError:
        print("{}: {}: Is a directory".format(command, filename))
    except FileNotFoundError:
        print("{}: {}: No such file or directory".format(command, filename))
    raise Exception()

def cat(args):
    for filename in args:
        try:
            with open_file('cat', filename) as f:
                for line in f:
                    stdout.write(line)
        except Exception:
            continue

LINES = 20
def more(args):
    for filename in args:
        header_len = max(len(filename), 20)+4
        print("*"*header_len)
        print("* {:^20} *".format(filename))
        print("*"*header_len)
        try:
            with open_file('more', filename) as f:
                counter = 0
                for line in f:
                    stdout.write(line)
                    counter += 1
                    if counter == LINES:
                        counter = 0
                        stdout.write("\x1b[1m*** Press Enter to continue... ***\x1b[0m\r")
                        input()
        except Exception:
            continue

def main(args):
    getattr(shell, args[0])(args[1:])

if __name__ == '__main__':
    main(sys.argv[1:])
